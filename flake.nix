{
  description = "Inivation libcaer";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        libcaer = pkgs.stdenv.mkDerivation {
          pname = "libcaer";
          version = "3.3.14";
          src = .;
          nativeBuildInputs = with pkgs; [
            pkg-config glibc libusb1 cmake flatbuffers ninja lz4
          ];
          preBuild = ''
            echo `pwd`
            echo ${pkgs.glibc}
            # substituteInPlace libcaer.pc --replace // :
            cat libcaer.pc
          '';
        };
      in
      rec {
        packages.default = libcaer;
      }
    );
}
